#  AnimCheckBox 

#### Introduction 

```
AnimCheckBox is a library that clicks the checkbox to change the animation effect.
```

#### Screenshots


![overview](screenShoot/animcheckbox.gif)

#### Installation

##### 方法1: Add the  package har

```
Packages up your library into a har，and add the har to the directory entry libs,

Add implementation codes to directory entry/gradle
implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
```

##### 方法2:  Add the gradle dependency

```
allprojects{
    repositories{
        mavenCentral()
    }
}
implementation "com.gitee.archermind-ti:AnimCheckBox:1.0.0-beta"
```


####  HOW TO USE 

1.  In Layout File 

  ```
<com.github.lguipeng.library.animcheckbox.AnimCheckBox
        ohos:height="match_content"
        ohos:width="80vp"
        ohos:layout_alignment="horizontal_center"
        ohos:padding="4vp"
        app:checked="true"
        app:circle_color="#1976D2"
        app:stroke_color="#2196F3"
        app:stroke_width="4vp"/>
  ```

2.  In Java File 

  ```
AnimCheckBox checkbox = (AnimCheckBox) findComponentById(ResourceTable.Id_checkbox)
checkbox.setChecked(true);
boolean animation = true;
checkbox.setChecked(false, animation);
  ```

####  License 

```
Copyright 2017 Liaoguipeng

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```

