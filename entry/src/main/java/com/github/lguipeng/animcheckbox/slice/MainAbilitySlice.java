package com.github.lguipeng.animcheckbox.slice;

import com.github.lguipeng.animcheckbox.ResourceTable;
import com.github.lguipeng.library.animcheckbox.AnimCheckBox;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

public class MainAbilitySlice extends AbilitySlice implements AnimCheckBox.OnCheckedChangeListener {
    private AnimCheckBox mAnimCheckBox1, mAnimCheckBox2;
    private final HiLogLabel lable = new HiLogLabel(HiLog.LOG_APP, 0x00201, "MainAbilitySlice");

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);

        mAnimCheckBox1 = (AnimCheckBox) findComponentById(ResourceTable.Id_checkbox_1);
        mAnimCheckBox2 = (AnimCheckBox) findComponentById(ResourceTable.Id_checkbox_2);
        mAnimCheckBox1.setOnCheckedChangeListener(this);
        mAnimCheckBox2.setOnCheckedChangeListener(this);
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    @Override
    public void onChange(AnimCheckBox view, boolean checked) {
        switch (view.getId()) {
            case ResourceTable.Id_checkbox_1:
                HiLog.debug(lable, "checkbox_1 -->" + checked);
                break;
            case ResourceTable.Id_checkbox_2:
                HiLog.debug(lable, "checkbox_2 --> " + checked);
                break;
        }
    }
}
